# TestTask

## Развертываем проект

  Создаем папку для проекта, например 
  
```sh
$ mkdir /var/www/public
```
Скачиваем проект
```sh
$ cd /var/www/public
```
```sh
$ git clone git@bitbucket.org:metasellerteam/testrepository.git ./
```
Инициализируем проект
```sh
$ php init --env=Development --overwrite=All
```
Устанавливаем vendors
```sh
$ composer install
```
Настраиваем Apache (тривиальный вариант):

```apache
    <VirtualHost *:80>
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/public/frontend/web
        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
        <Directory "/var/www/public/frontend/web">
            Options Indexes FollowSymLinks
            AllowOverride all
            Require all granted
        </Directory>
    </VirtualHost>
   ```
Не забываем перезапустить Apache:
```sh
$ sudo /etc/init.d/apache2 restart
```
Тестовое задание доступно по адресу:

http://<your_server_ip_address>/api/v1/buttons