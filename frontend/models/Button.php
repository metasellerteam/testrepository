<?php
namespace frontend\models;

use yii\base\Model;

/**
 * Signup form
 */
class Button extends Model
{
    public $id;
    public $type;
    public $value;
    protected $secretValue;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'type', 'value', 'secretValue'], 'required'],
            ['id', 'integer'],
            [['type', 'value'], 'string'],
            ['secretValue', 'string', 'min' => 2, 'max' => 255],

        ];
    }

    protected static function testString($prefix = '', $length = 7){
        return $prefix.substr(md5(mt_rand()), 0, $length);
    }

    public static function createTestButton(){

        $button = new static(); // В ключе нашей беседы :)

        $button->id = rand();

        $button->type = static::testString('type');
        $button->value = static::testString('value');
        $button->secretValue = static::testString('secretValue');

        return $button;
    }

    public static function generateArrayOfTestButtons($cnt = 5){

        $arrayOfButtons = [];

        for ($i = 0; $i<$cnt; $i++){
            $arrayOfButtons[] = static::createTestButton();

        }

        return $arrayOfButtons;
    }

}
