<?php

namespace frontend\controllers\api\v1;

use frontend\controllers\api\BaseApiController;
use frontend\models\Button;

class ButtonsController extends BaseApiController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        return $behaviors;
    }

    public function actionIndex(){

        return Button::generateArrayOfTestButtons(5);
    }

}