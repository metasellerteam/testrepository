<?php

namespace frontend\controllers\api;

use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;
use yii\web\Response;

class BaseApiController extends Controller
{
    const STATUS_ERROR_CODE = 422;

    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;

        $behaviors = ArrayHelper::merge([
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['GET'],
                ],
            ]
        ], $behaviors);

        return $behaviors;
    }

    public function init()
    {
        parent::init();

        \Yii::$app->set('response', [
            'class' => 'yii\web\Response',
            'format' => Response::FORMAT_JSON,
            'formatters' => [
                Response::FORMAT_JSON => [
                    'class' => 'yii\web\JsonResponseFormatter',
                    'prettyPrint' => YII_DEBUG, // use "pretty" output in debug mode
                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
                ],
            ],
            //'charset' => 'UTF-8'
            'on beforeSend' => function ($event) {
                static::responseBeforeSend($event);
            },
        ]);
    }

    /**
     * Response with error status code 422
     * @link https://github.com/yiisoft/yii2/blob/master/docs/guide-ru/rest-error-handling.md
     * @param $errors array errors array
     * @return array array to be published
     */
    protected function responseErrors($errors)
    {
        Yii::$app->response->setStatusCode(static::STATUS_ERROR_CODE);
        return [
            'errors' => $errors
        ];
    }

    /**
     * Reformat response
     * @link https://www.yiiframework.com/doc/guide/2.0/en/runtime-handling-errors#customizing-error-response-format
     * @param $event
     */
    public static function responseBeforeSend($event)
    {
        $response = $event->sender;
        if ($response->data !== null) {
            $response->data = [
                'success' => $response->isSuccessful,
                'data' => $response->data,
            ];
        }
    }

}